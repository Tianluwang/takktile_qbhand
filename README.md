# takktile_qbhand
The file introduces how to integrate takktile sensors related software to your ROS environment.

Maintainer(s): Tianlu Wang ([tiawang@student.ethz.ch](tiawang@student.ethz.ch))

## Requirement
### Software
Ubuntu 16.04,
ROS Kinetic.
### Hardware
Takktile TakkStrip sensor array,
Takktile TakkFast ROS interface.

## Integration
### Installing USB drivers
1) Check that the device enamurates

Plug the TakkFast to the computer, open terminal window and type:
```
lsusb
```
and see if there is a device labeled by 59e3:74c7.

2) Download the driver related code
```
git clone --recursive git://github.com/TakkTile/TakkTile-usb.git
```

3) Install the PyUSB-1.0 Python module
```
cd TakkTile-usb/PyUSB
```
```
sudo python setup.py install
```

4) Update permissions
```
sudo cp 71-takktile.rules /etc/udev/rules.d/
```

5) Run the example 
```
cd TakkTile-usb
```
```
sudo python TakkTile.py
```
The data from the sensors should be now shown in the terminal screen.

### Build ROS package
1) Get the ROS package and build it
```
git clone git@bitbucket.org:Tianluwang/takktile_qbhand.git
```
```
catkin build takktile_ros
```

2) Source the catkin_ws
```
source devel/setup.bash
```

3) Set USB permissions
```
cd takktile_ros
```
```
sudo cp 71-takktile.rules /etc/udev/rules.d/
```

## Viewing topics
1) Launch the node get access to sensor measurement data
```
roslaunch takktile_ros takktile_ros.launch
```

2) To zero out sensor drift
```
rosservice call /takktile/zero
```
## Debugging
1) If after launching, error exists. Make sure that all the sensor boards are connected properly. Unplug the USB calble, then plug in again. Launch the node once more. 





